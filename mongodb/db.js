import { default as mongodb } from "mongodb";
let MongoClient = mongodb.MongoClient;
var url = "mongodb://localhost/testdb";
let database;

MongoClient.connect(url, function (err, db) {
  if (err) throw err;
  database = db.db("testdb");
});

const getEndDate = async function () {
  return await database.collection("countdown").findOne();
};

const getRawCryptoData = async function () {
  return await database.collection("rawCryptoData").findOne();
};

const getPreprocessedCryptoData = async function () {
  return await database.collection("processedCryptoData").findOne();
};

const getEvaluatedCryptoData = async function () {
  return await database.collection("evaluatedCryptoData").findOne();
};

const getTopTenEvaluatedCryptos = async function () {
  return await database.collection("topTenEvaluatedCryptos").findOne();
};

const updateEvaluatedCryptoData = async function (evaluatedData) {
  await database.collection("evaluatedCryptoData").deleteMany({});
  return await database
    .collection("evaluatedCryptoData")
    .insertOne({ list: evaluatedData });
};

const updateNewEndDate = async function (endDate) {
  await database.collection("countdown").deleteMany({});
  return await database.collection("countdown").insertOne({ endDate: endDate });
};

const updateRawCryptoData = async function (cryptoInformationList) {
  await database.collection("rawCryptoData").deleteMany({});
  return await database
    .collection("rawCryptoData")
    .insertOne({ list: cryptoInformationList });
};

const updatePreprocessedCryptoData = async function (cryptoInformationList) {
  await database.collection("processedCryptoData").deleteMany({});
  return await database
    .collection("processedCryptoData")
    .insertOne({ list: cryptoInformationList });
};

const updateTopTenEvaluatedCryptos = async function (topTen) {
  await database.collection("topTenEvaluatedCryptos").deleteMany({});
  return await database
    .collection("topTenEvaluatedCryptos")
    .insertOne({ list: topTen });
};

export default {
  updateRawCryptoData,
  updateNewEndDate,
  updateEvaluatedCryptoData,
  getEvaluatedCryptoData,
  getEndDate,
  updateTopTenEvaluatedCryptos,
  getTopTenEvaluatedCryptos,
  updatePreprocessedCryptoData,
  getPreprocessedCryptoData,
  getRawCryptoData,
};
