# Crypto Bot

### Mac install

```sh
$ brew tap mongodb/brew
$ brew install mongodb-community@4.4
$ brew services start mongodb-community@4.4
$ npm install
$ node index.js
```
### Windows install
```sh
$ npm install
$ Download MongoDB for Windows @ https://www.mongodb.com/try/download/community
$ New connection
$ Connect
$ node index.js
```
