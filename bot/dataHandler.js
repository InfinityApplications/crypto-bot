import cryptoIndicators from "./cryptoIndicatorsCalculation.js";
import axios from "axios";
import config from "../config.js";
import db from "../mongodb/db.js";

/**
 * QUERIES THE API TO UPDATE THE CRYPTO DATA IN THE DATABASE
 *
 *  @return {Boolean} STATUS OF THE UPDATE.
 */
async function fetchAndProcessRawCryptoData() {
  let rawData = [];
  let et = Math.round(new Date().getTime() / 1000) - 60 * 60 * 24 * 7;
  let ts = Math.round(new Date().getTime() / 1000);
  for await (let cryptoPair of config.cryptoList) {
    await axios
      .get(
        "https://www.binance.com/api/v3/klines?symbol=" +
          cryptoPair.name +
          "BTC" +
          "&interval=4h&startTime=" +
          ts
      )
      .then((value) => {
        let dataObject = Object.create({});
        dataObject.cryptoPair = cryptoPair.name;
        dataObject.value = cryptoPair.value;
        dataObject.data = value.data;
        rawData.push(dataObject);
      });
  }
  await db.updateRawCryptoData(rawData);
  await processRawCryptoData();
  await evaluatePreprocessedCryptoData();
  // await updateTopTenEvaluatedCryptos();
  return;
}

/**
 * FETCHES THE RAW DATA FROM THE DATABASE TO PROCESS THE INDICATORS AND UPDATE THE DATABASES PROCESSED DATA
 *
 * @return {Boolean} STATUS OF THE UPDATE.
 */
async function processRawCryptoData() {
  let cryptoListOfData = await db.getRawCryptoData();
  let processedCryptoData = [];
  cryptoListOfData.list.forEach((candleData) => {
    let dataObject = Object.create({});
    dataObject.name = candleData.cryptoPair;
    dataObject.value = candleData.value;
    // dataObject.sma = cryptoIndicators.calculateSMA(candleData.data);
    // dataObject.bollangerBands = cryptoIndicators.calculateBollangerBands(candleData.data);
    processedCryptoData.push(dataObject);
  });
  await db.updatePreprocessedCryptoData(processedCryptoData);

  return;
}

/**
 * EVALUATES EACH CRYPTO FROM THE CONFIG FILE GIVING A NUMBER BETWEEN 0 AND 1
 *
 * @return {Boolean} STATUS OF THE UPDATE.
 */
async function evaluatePreprocessedCryptoData() {
  let preProcessedData = await db.getPreprocessedCryptoData();
  let evaluatedData = getRandom(preProcessedData.list, 10);
  // TODO EVALUATION FUNCTION
  await db.updateTopTenEvaluatedCryptos(evaluatedData);
  return;
}

function getRandom(arr, n) {
  let topTenArray = [];
  while (topTenArray.length < n) {
    let item = arr[Math.floor(Math.random() * arr.length)];
    if (!topTenArray.includes(item)) {
      topTenArray.push(item);
    }
  }
  return topTenArray;
}

// /**
//  * TAKES THE EVALUATION OF EACH CRYPTO AND SORTS IT BASED ON ITS EVALUATED NUMBER THEN STORES THE TOP TEN
//  *
//  * @return {Boolean} STATUS OF THE UPDATE.
//  */
// async function updateTopTenEvaluatedCryptos() {
//   let evaluatedData = await db.getPreprocessedCryptoData();
//   evaluatedData.list.sort(function (evaluationOne, evaluationTwo) {
//     return evaluationOne.evaluation > evaluationTwo.evaluation;
//   });
//   evaluatedData = evaluatedData.list.slice(0, 10);
//   let topTenArray = []
//   evaluatedData.forEach(value => {
//     let dataObject = Object.create({});
//     dataObject.name = value.cryptoPair,
//       dataObject.value = value.value;
//     topTenArray.push(dataObject)
//   })

//   return await db.updateTopTenEvaluatedCryptos(topTenArray);
// }

export default {
  fetchAndProcessRawCryptoData,
  evaluatePreprocessedCryptoData,
  processRawCryptoData,
};
