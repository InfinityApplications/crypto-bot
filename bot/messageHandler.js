import db from "../mongodb/db.js";
import textToImage from "text-to-image";

const handle = async function (message, client) {
  let messageArray = message.content.split(" ");

  if (messageArray[0] === "!start-timer") {
    if (Date.parse(messageArray[1])) {
      console.log(messageArray[1]);
      console.log(new Date(messageArray[1]));
      db.updateNewEndDate(Date.parse(messageArray[1]));
    }
  } else if (messageArray[0] === "!embed") {
    message.channel.send({
      embed: {
        color: 9447005,
        author: {
          name: "When the countdown is up the following function is called",
          icon_url: client.user.avatarURL
        },
        image: {
          url: 'https://i.ibb.co/PQFdqh1/Screenshot-2021-02-12-at-23-23-31.png',
        },
        footer: {
          icon_url: client.user.avatarURL,
          text: "© APS",
        },
      },
    });
  } else if (messageArray[0] === "!KARU-IS-GAY") {
    //addition logic
  } else if (messageArray[0] === "!KARU-IS-GAY") {
    //addition logic
  }
};

async function sendTopTen(client, data) {
  client.channels.cache.get("808966009079070761").send({
    embed: {
      color: 9447005,
      author: {
        name: client.user.username,
        icon_url: client.user.avatarURL,
      },
      title: "TOP TEN CRYPTOS LOOKING TO PUMP",
      description:
        "Here are the Anlytical Pump Signals top ten pump watch for the day.",
      fields: data.list,
      timestamp: new Date(),
      footer: {
        icon_url: client.user.avatarURL,
        text: "© APS",
      },
    },
  });
}

async function sendPumpSignal(client, signal) {
  client.channels.cache.get("797591808921042984").send({
    embed: {
      color: 3447003,
      author: {
        name: client.user.username,
        icon_url: client.user.avatarURL,
      },
      title: "This weeks pump signal is...",
      fields: signal,
      footer: {
        icon_url: client.user.avatarURL,
        text: "© APS",
      },
    },
  });
}

export default { handle, sendPumpSignal, sendTopTen };
