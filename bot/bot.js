import { Client } from "discord.js";
import moment from "moment";
import db from "../mongodb/db.js";
import messageHandler from "./messageHandler.js";
import dataHandler from "./dataHandler.js";
import config from "../config.js";

export const client = new Client();

client.login(config.apiKey);

client.once("ready", () => {
  //LOGGING TO CONSOLE
  console.log(`Initialized client`);

  //SET INTERVAL PASSING IN computeActivityWatching() AS THE FUNCTION TO BE CALLED EVERY 10 SECONDS
  setInterval(async () => {
    client.user.setActivity(await computeActivityWatching()); //SETS BOTS ACTIVITY TO WHAT IS RETURNED BY THE MAIN FUNCTION.
  }, 3000);

  //SET INTERVAL PASSING IN fetchCryptoData() AS THE FUNCTION TO BE CALLED EVERY 24 HOURS
  setInterval(async () => {
    computeTopTen(); //FETCHES CRYPTO DATA
  }, 120000); //THIS NUMBER REPRESENTS 24 HOURS
});

/**
 * ON MESSAGE, CALL THE MESSAGE HANDLER, PASS THAT LOGIC ELSEWHERE BITCH
 *
 * @param {Object} message INCOMING MESSAGE OBJECT FROM DISCROD
 * @return {number} DISCORD MESSAGE
 */
client.on("message", async (message) => {
  await messageHandler.handle(message, client);
});

/**
 * CALLED EVERY 10 SECONDS, HERE LIES THE MAIN LOGIC FOR WHAT THE BOT IS WATCHING
 *
 * @return {String} STATUS OF WHAT THE BOT IS WATCHING.
 */
async function computeActivityWatching() {
  try {
    const date = await db.getEndDate(); //FETCH ENDDATE FROM MONGODB
    if (date === null) {
      return "waiting";
    }
    if (moment().isValid(new Date(date.endDate))) {
      const timeLeft = computeTimeLeft(date.endDate)._data;
      if (
        timeLeft.seconds < 30 &&
        timeLeft.minutes <= 0 &&
        timeLeft.hours <= 0
      ) {
        await db.updateNewEndDate(
          moment().add(7, "DAYS").format("YYYY-MM-DD hh:mm:ss")
        );
        await choosePumpSignal();
      } else {
        let timeLeft = computeTimeLeft(date.endDate);
        return parseTimeLeft(timeLeft);
      }
    }
  } catch (e) {
    console.error(e);
  }
}

/**
 * CALLED EVERY 24 HOURS
 *
 * @return {String} STATUS OF WHAT THE BOT IS WATCHING.
 */
async function computeTopTen() {
  try {
    await dataHandler.fetchAndProcessRawCryptoData();
    const data = await db.getTopTenEvaluatedCryptos();
    console.log(data);
    messageHandler.sendTopTen(client, data);
  } catch (e) {
    console.error(e);
  }
}

/**
 * COMPUTES THE TIME DIFFERENCE BETWEEN TWO DATES MAKING US OF THE "ComputeTimeDiff" IMPORTED METHOD
 * @return {String} STATUS OF WHAT THE BOT IS WATCHING.
 */
function computeTimeLeft(endDate) {
  const date = moment(moment(new Date(endDate)).format("YYYY-MM-DD hh:mm:ss"));
  const dateFormatted = moment(new Date(date));
  const duration = moment.duration(
    dateFormatted.diff(moment().format("YYYY-MM-DD hh:mm:ss"))
  );

  return duration;
}

/**
 * @return String
 */
async function parseTimeLeft(duration) {
  const days = duration._data.days + " days ";
  const hours = duration._data.hours + " hours ";
  const minutes = duration._data.minutes + " minutes ";
  const seconds = duration._data.seconds + " seconds ";

  return days + hours + minutes + seconds;
}

/**
 * TAKES THE TOP TEN EVALUATED CRYPTOS AND CHOOSES ONE AT RANDOM
 *
 * @return {Object} RANDOM CRYPTO SIGNAL TO PUMP BIIIIITCH
 */
async function choosePumpSignal() {
  try {
    let topTenEvaluatedCryptos = await db.getTopTenEvaluatedCryptos();
    let randomSignal =
      topTenEvaluatedCryptos.list[
        Math.floor(Math.random() * topTenEvaluatedCryptos.list.length)
      ];
    console.log(topTenEvaluatedCryptos);
    messageHandler.sendPumpSignal(client, randomSignal);
  } catch (e) {
    console.error(e);
  }
}
