import { default as TechnicalIndicators } from "technicalindicators";
let ti = TechnicalIndicators;

function calculateSMA(candles) {
  return ti.EMA.calculate({
    period: 15,
    values: formatCandles(candles).close,
  });
}

function calculateBollangerBands(candles) {
  let data = ti.BollingerBands.calculate({
    period: 1,
    values: formatCandles(candles).close,
    stdDev: 2,
  });
  return data;
}

function formatCandles(candles) {
  let format = {
    open: [],
    close: [],
    high: [],
    low: [],
    vol: [],
  };

  // reverse candles as oldest candle must given first 'technicalindicators' lib
  candles.reverse().forEach((candle) => {
    format.open.push(candle[1]);
    format.high.push(candle[2]);
    format.low.push(candle[3]);
    format.close.push(candle[4]);
    format.vol.push(candle[5]);
  });
  return format;
}

export default { calculateBollangerBands, calculateSMA };
